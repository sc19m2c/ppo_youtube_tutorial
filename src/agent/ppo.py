import torch


class PPO(torch.nn.module):

    def __init__(
        self,
        action_space = 5,
        state_space = 20,
        network = {'fe':[32,32], 'actor':[32, 16], 'critic':[16,1]},
        
        clip = 0.2,
        value_coefficient = 1e-2,
        entropy_coefficient = 1e-5,
        gae_lambda = 0.95,

        learning_rate = 0.001,
        gamma = 0.99,
        lr_decay = None,

        batch_size = 4000,
        epochs = 5,

        action_type = 'continous' # ['continuos', 'discrete']

        ):
        self.action_space = action_space
        self.state_space = state_space
        self.network = network
        self.clip = clip
        self.value_coefficient = value_coefficient
        self.entropy_coefficient = entropy_coefficient
        self.gae_lambda = gae_lambda
        self.learning_rate = learning_rate
        self.gamma = gamma
        self.lr_decay = lr_decay
        self.batch_size = batch_size
        self.epochs = epochs


        self.feature_extractor = torch.nn.Linear(state_space, 32)
        self.critic = torch.nn.Linear(32,1)
        self.actor = torch.nn.Linear(32,action_space)

        return


    def _actions(self, state):
        """
        @param state: the observation of the agent in the environemnt

        @return the action selected by algorithim
        """

        features = self.feature_extractor(state)
        



        return 

    def _update(self, experience):
        """
        @param experience: data collected during rollout of the agent [episode, [s, a, s', r] * rollout steps]
        
        experiences must be grouped by the episode 

        for each time step we need:
        A_t : an generalized advantage estimate
        V_trg_t : the true sum of discounted rewards
        a_t : the action taken
        s_t : the given state
        """

        # compute A_t, V_t
        for episode in len(experience):

            for t in reversed(len(experience[episode])):


