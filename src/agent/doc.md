

# generalized advantage estimate 
- https://arxiv.org/pdf/1506.02438.pdf HIGH-DIMENSIONAL CONTINUOUS CONTROL USING GENERALIZED ADVANTAGE ESTIMATION

- $\hat{A}^{GAE(\gamma, \lambda)}_t = \sum^{\inf}_{l=0} (\gamma \lambda)^l \delta^V_{t+l}$
- where $\delta^V_{t+1} = -V(s_t) + r_t + \gamma V(s_{t+1})$



# entropy term  
- https://arxiv.org/pdf/1602.01783v2.pdf Asynchronous Methods for Deep Reinforcement Learning

- The gradient of the full objective function including the entropy
regularization term with respect to the policy parameters takes the form $∇
log π(a_t|s_t)(R_t − V (s_t; θ_v)) + β∇θ H(π(s_t; θ))$, where $H$ is the entropy. The hyperperameter $β$ controls the strength of the entropy regularization term

- entropy = $E[-log(p(x))]$ = 
- entropy of a normal $N(\mu, \sigma)$ distribution - e.g. continuous action spaces: 
- $H = \int^{\inf}_{-\inf}p(x)-log(p(x))dx$
- $H = \int^{\inf}_{-\inf}p(x)+(1/2 log(2\pi v)+ 1/2v (x-\mu)^2)dx$
- $H = 1/2 log(2\pi v) \ ( \int^{\inf}_{-\inf}p(x) + 1/2v \int^{\inf}_{-\inf} (x-\mu)^2)dx)$
- $H = 1/2 log(2\pi v) \ (1 + 1/2v \ v)$
- $H = 1/2(log(2 \pi v)+1)$